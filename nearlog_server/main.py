from flask import Flask, render_template
import json
import os
import time


app = Flask(__name__)

read_time = 0
data      = []

def _get_data():
  with open('./data.json') as file:
    data = json.load(file)
    read_time = int(time.time())
  return data

def get_data():
  mod_time = os.path.getmtime('./data.json')
  if mod_time > read_time:
    return _get_data()
  else:
    return data

def _save_data(data):
  with open('./data.json', 'w') as file:
    json.dump(data, file)

def add_data(value, cause):
  data = get_data()
  data.append([value, cause])
  _save_data(data)


@app.route('/')
def index():
  return render_template('index.html')

@app.route('/about')
def about():
  return render_template('about.html')

@app.route('/data.json')
def data_json():
  data = get_data()
  return json.dumps(data)

@app.route('/submit/<uuid:uuid>/<cause>')
def submit(uuid, cause):
  if [str(uuid), str(cause)] in get_data():
    return 'error_duplicate'
  try:
    add_data(str(uuid), str(cause))
  except Exception as err:
    return 'error_unknown;{};{}'.format(type(err), str(err))
  else:
    return 'success_original'

@app.route('/data.html')
def data_html():
  data = get_data()
  return render_template('data.html', data = data, data_len = len(data))


if __name__ == '__main__':
  app.run(host='0.0.0.0', port=8080, debug=False, use_evalex=False)